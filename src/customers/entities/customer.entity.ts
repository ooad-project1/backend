
import { CartItem } from 'src/cart_items/entities/cart_item.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { UserCus } from 'src/user_cus/entities/user_cus.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Customer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  lastName: string;

  @Column()
  tel: string;

  @Column()
  startDate: string;

  @Column()
  point: number;

  @Column()
  userCusId: number;

  @OneToOne(() => UserCus, (userCus) => userCus.customer)
  @JoinColumn()
  userCus: UserCus;

  @OneToMany(() => Receipt, (receipt) => receipt.customer)
  receipts: Receipt[];

  @OneToMany(() => CartItem, (cartItem) => cartItem.customer)
  cartItem: CartItem[];
}
