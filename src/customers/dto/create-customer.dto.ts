import { IsNotEmpty } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  lastName: string;

  @IsNotEmpty()
  tel: string;

  @IsNotEmpty()
  startDate: string;

  @IsNotEmpty()
  point: number;

  // @IsNotEmpty()
  userCusId: number;
}
