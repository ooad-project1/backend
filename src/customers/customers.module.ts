import { Module } from '@nestjs/common';
import { CustomersService } from './customers.service';
import { CustomersController } from './customers.controller';
import { Customer } from './entities/customer.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserCus } from 'src/user_cus/entities/user_cus.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { CartItem } from 'src/cart_items/entities/cart_item.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Customer, UserCus, Receipt, CartItem])],
  controllers: [CustomersController],
  providers: [CustomersService],
})
export class CustomersModule { }
