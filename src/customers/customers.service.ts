import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customer } from './entities/customer.entity';
import { UserCus } from 'src/user_cus/entities/user_cus.entity';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
    @InjectRepository(UserCus)
    private userCusRepository: Repository<UserCus>,
  ) { }

  async create(createCustomerDto: CreateCustomerDto) {
    const newCustomer = new Customer();
    newCustomer.name = createCustomerDto.name;
    newCustomer.lastName = createCustomerDto.lastName;
    newCustomer.tel = createCustomerDto.tel;

    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, '0');
    const day = String(currentDate.getDate()).padStart(2, '0');
    const hours = String(currentDate.getHours()).padStart(2, '0');
    const minutes = String(currentDate.getMinutes()).padStart(2, '0');
    const seconds = String(currentDate.getSeconds()).padStart(2, '0');
    const formattedDateTime = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
    newCustomer.startDate = formattedDateTime;

    // newCustomer.startDate = new Date().toLocaleDateString();
    newCustomer.point = createCustomerDto.point;

    const userCus = await this.userCusRepository.findOne({
      where: { id: createCustomerDto.userCusId },
    });
    if (!userCus) {
      throw new NotFoundException();
    }
    newCustomer.userCus = userCus;
    return this.customersRepository.save(createCustomerDto);
  }

  findAll() {
    return this.customersRepository.find({
      select: {
        userCus: { login: true, name: true, lastName: true, tel: true, email: true },
      },
      relations: ['userCus']
    });
  }

  async findOne(id: number) {
    const customer = await this.customersRepository.findOne({
      where: { id: id },
      relations: ['receipts'],
    });
    if (!customer) {
      throw new NotFoundException();
    }
    return customer;
  }

  async findCusByTel(tel: string) {
    console.log(tel);
    const customer = await this.customersRepository.findOne({
      where: { tel: tel },
    });
    if (!customer) {
      throw new NotFoundException();
    }
    return customer;
  }

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    const customer = await this.customersRepository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    const updatedCustomer = { ...customer, ...updateCustomerDto };
    return this.customersRepository.save(updatedCustomer);
  }

  async remove(id: number) {
    const customer = await this.customersRepository.findOneBy({ id: id });
    if (!customer) {
      throw new NotFoundException();
    }
    return this.customersRepository.remove(customer);
  }
}
