import { Customer } from "src/customers/entities/customer.entity";
import { Product } from "src/products/entities/product.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class CartItem {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    productId: number;

    @Column()
    Qty: number;

    @Column()
    customerId: number;

    @ManyToOne(() => Product, (product) => product.cartItem)
    product: Product;

    @ManyToOne(() => Customer, (customer) => customer.cartItem)
    customer: Customer;
}
