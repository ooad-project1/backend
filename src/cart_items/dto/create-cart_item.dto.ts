import { IsNotEmpty } from "class-validator";


export class CreateCartItemDto {
    @IsNotEmpty()
    productId: number;

    @IsNotEmpty()
    Qty: number;

    @IsNotEmpty()
    customerId: number;


}
