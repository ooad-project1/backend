import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCartItemDto } from './dto/create-cart_item.dto';
import { UpdateCartItemDto } from './dto/update-cart_item.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { CartItem } from './entities/cart_item.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Product } from 'src/products/entities/product.entity';

@Injectable()
export class CartItemsService {
  constructor(
    @InjectRepository(CartItem)
    private cartItemsRepository: Repository<CartItem>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
  ) { }

  async create(createCartItemDto: CreateCartItemDto) {
    const product = await this.productsRepository.findOne({
      where: {
        id: createCartItemDto.productId,
      },
    });

    const customer = await this.customerRepository.findOne({
      where: {
        id: createCartItemDto.customerId,
      },
    });
    const newCarItem = new CartItem();
    newCarItem.Qty = createCartItemDto.Qty;
    newCarItem.customerId = createCartItemDto.customerId;
    newCarItem.productId = createCartItemDto.productId;
    return this.cartItemsRepository.save(newCarItem);
  }

  findAll() {
    return this.cartItemsRepository.find({
      select: {
        product: { id: true },
        customer: { id: true },
      },
      relations: ['product', 'customer']
    });
  }

  findOne(id: number) {
    return this.cartItemsRepository.findOne({
      where: { id: id },
      select: {
        customer: { id: true },
      },
      relations: ['customer']
    });
  }

  async findOneByCustomerId(customerId: number) {
    return this.cartItemsRepository.find({
      where: { customerId: customerId },
      relations: ['product', 'customer'] // หากต้องการข้อมูลของสินค้าและลูกค้าด้วย
    });
  }

  async update(id: number, updateCartItemtDto: UpdateCartItemDto) {
    const cartItem = await this.cartItemsRepository.findOne({
      where: { id: id },
    });
    if (!cartItem) {
      throw new NotFoundException();
    }
    Object.assign(cartItem, updateCartItemtDto);
    return this.cartItemsRepository.save(cartItem);
  }

  async remove(id: number) {
    const cartItem = await this.cartItemsRepository.findOne({
      where: { id: id },
    });
    if (!cartItem) {
      throw new NotFoundException();
    }
    return this.cartItemsRepository.remove(cartItem);
  }

  async deleteCartItemsByCustomerId(customerId: number): Promise<DeleteResult> { // ระบุให้เมธอด return Promise<DeleteResult>
    // ลบรายการสินค้าที่เกี่ยวข้องกับลูกค้านี้ออกจากตะกร้า
    return await this.cartItemsRepository.delete({ customerId }); // ให้เมธอด return ค่าที่ได้จากการลบรายการสินค้า
  }

}
