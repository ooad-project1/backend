import { Module } from '@nestjs/common';
import { CartItemsService } from './cart_items.service';
import { CartItemsController } from './cart_items.controller';
import { Employee } from 'src/employee/entities/employee.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from 'src/products/entities/product.entity';
import { CartItem } from './entities/cart_item.entity';
import { Customer } from 'src/customers/entities/customer.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Product, CartItem, Customer])],
  controllers: [CartItemsController],
  providers: [CartItemsService]
})
export class CartItemsModule { }
