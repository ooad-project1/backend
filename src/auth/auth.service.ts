import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserCus } from 'src/user_cus/entities/user_cus.entity';
import { UserCusService } from 'src/user_cus/user_cus.service';
import { UserEmp } from 'src/user_emp/entities/user_emp.entity';
import { UserEmpService } from 'src/user_emp/user_emp.service';


@Injectable()
export class AuthService {
    constructor(private userCusService: UserCusService, private userEmpService: UserEmpService, private jwtService: JwtService,) { }
    async validateUser(username: string, pass: string): Promise<any> {
        const userCus = await this.userCusService.findOneByEmail(username);
        const userEmp = await this.userEmpService.findOneByEmail(username);
        if (userCus && userCus.password === pass) {
            const { password, ...result } = userCus;
            return result;
        }
        if (userEmp && userEmp.password === pass) {
            const { password, ...result } = userEmp;
            return result;
        }
        return null;
    }

    async login(user: UserCus | UserEmp) {
        const payload = {
            username: user.email,
            sub: user.id,
        };
        return {
            user,
            access_token: this.jwtService.sign(payload),
        };
    }

}
