import { Module } from '@nestjs/common';
import { BillDetailsService } from './bill_details.service';
import { BillDetailsController } from './bill_details.controller';
import { BillDetail } from './entities/bill_detail.entity';
import { Bill } from 'src/bills/entities/bill.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from 'src/products/entities/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([BillDetail, Bill, Product])],
  controllers: [BillDetailsController],
  providers: [BillDetailsService]
})
export class BillDetailsModule { }
