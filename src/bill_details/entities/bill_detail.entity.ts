import { Bill } from "src/bills/entities/bill.entity";
import { Product } from "src/products/entities/product.entity";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class BillDetail {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    detailName: string;

    @Column()
    detailPrice: number;

    @Column()
    detailAmount: number;

    @Column()
    detailTotal: number;

    @Column()
    productId: number;

    @Column()
    employeeId: number;

    @ManyToOne(() => Bill, (bill) => bill.billDetail)
    bill: Bill;

    @ManyToOne(() => Product, (product) => product.billDetails) // กำหนดความสัมพันธ์แบบ Many-to-One ระหว่าง BillDetail และ Product
    product: Product;












}
