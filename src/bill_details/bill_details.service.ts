import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBillDetailDto } from './dto/create-bill_detail.dto';
import { UpdateBillDetailDto } from './dto/update-bill_detail.dto';
import { BillDetail } from './entities/bill_detail.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from 'src/products/entities/product.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { Bill } from 'src/bills/entities/bill.entity';

@Injectable()
export class BillDetailsService {
  constructor(
    @InjectRepository(BillDetail)
    private billDetailsRepository: Repository<BillDetail>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Bill)
    private billsRepository: Repository<Bill>
  ) { }





  async create(createBillDetailDto: CreateBillDetailDto) {
    const product = await this. productsRepository.findOneBy({
      id: createBillDetailDto.productId ,
    });
    const bill = await this.billsRepository.findOneBy({
      id: createBillDetailDto.billId,
    });

    const newBillDetail = new BillDetail();
    newBillDetail.detailName = createBillDetailDto.detailName;
    newBillDetail.detailPrice = createBillDetailDto.detailPrice;
    newBillDetail.detailAmount = createBillDetailDto.detailAmount;
    newBillDetail.detailTotal = createBillDetailDto.detailTotal;
    newBillDetail.product = product;
    newBillDetail.bill = bill;

  }
 
  findAll() {
    return this.billDetailsRepository.find({
      select: {
        product: { name: true },
        
      },
      relations: ['product']
    });
  }

  findOne(id: number) {
    return this.billDetailsRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updatebillDetailDto: UpdateBillDetailDto) {
    try {
      const updatedBillDetail = await this.billDetailsRepository.save({
        id,
        ...updatebillDetailDto,
      });
      return updatedBillDetail;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const billDetail = await this.billDetailsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedBillDetail = await this.billDetailsRepository.remove(billDetail);
      return deletedBillDetail;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
