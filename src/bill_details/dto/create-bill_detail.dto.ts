import { IsNotEmpty } from "class-validator";

export class CreateBillDetailDto {
    @IsNotEmpty()
    detailName: string
    
    @IsNotEmpty()
    detailPrice: number;

    @IsNotEmpty()
    detailAmount: number;

    @IsNotEmpty()
    detailTotal: number;

    @IsNotEmpty()
    productId: number;

    @IsNotEmpty()
    billId: number;
}
