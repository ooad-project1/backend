import { Test, TestingModule } from '@nestjs/testing';
import { CheckProductDetailsController } from './check_product_details.controller';
import { CheckProductDetailsService } from './check_product_details.service';

describe('CheckProductDetailsController', () => {
  let controller: CheckProductDetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckProductDetailsController],
      providers: [CheckProductDetailsService],
    }).compile();

    controller = module.get<CheckProductDetailsController>(CheckProductDetailsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
