import { Injectable } from '@nestjs/common';
import { CreateCheckProductDetailDto } from './dto/create-check_product_detail.dto';
import { UpdateCheckProductDetailDto } from './dto/update-check_product_detail.dto';

@Injectable()
export class CheckProductDetailsService {
  create(createCheckProductDetailDto: CreateCheckProductDetailDto) {
    return 'This action adds a new checkProductDetail';
  }

  findAll() {
    return `This action returns all checkProductDetails`;
  }

  findOne(id: number) {
    return `This action returns a #${id} checkProductDetail`;
  }

  update(id: number, updateCheckProductDetailDto: UpdateCheckProductDetailDto) {
    return `This action updates a #${id} checkProductDetail`;
  }

  remove(id: number) {
    return `This action removes a #${id} checkProductDetail`;
  }
}
