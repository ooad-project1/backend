import { CheckProduct } from "src/check_products/entities/check_product.entity";
import { Product } from "src/products/entities/product.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class CheckProductDetail {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    QtyLast: number;

    @Column()
    QtyRemain: number;

    @Column()
    QtyDamaged: string;

    @Column()
    productId: number;

    @Column()
    cpId: number;

    @ManyToOne(() => Product, (product) => product.checkProductDetail)
    product: Product;

    @ManyToOne(() => CheckProduct, (checkProduct) => checkProduct.checkProductDetail)
    checkProduct: CheckProduct;



}   
