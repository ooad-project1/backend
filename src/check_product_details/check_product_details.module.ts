import { Module } from '@nestjs/common';
import { CheckProductDetailsService } from './check_product_details.service';
import { CheckProductDetailsController } from './check_product_details.controller';
import { CheckProductDetail } from './entities/check_product_detail.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from 'src/products/entities/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CheckProductDetail, Product])],
  controllers: [CheckProductDetailsController],
  providers: [CheckProductDetailsService]
})
export class CheckProductDetailsModule { }
