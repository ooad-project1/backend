import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CheckProductDetailsService } from './check_product_details.service';
import { CreateCheckProductDetailDto } from './dto/create-check_product_detail.dto';
import { UpdateCheckProductDetailDto } from './dto/update-check_product_detail.dto';

@Controller('check-product-details')
export class CheckProductDetailsController {
  constructor(private readonly checkProductDetailsService: CheckProductDetailsService) {}

  @Post()
  create(@Body() createCheckProductDetailDto: CreateCheckProductDetailDto) {
    return this.checkProductDetailsService.create(createCheckProductDetailDto);
  }

  @Get()
  findAll() {
    return this.checkProductDetailsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkProductDetailsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCheckProductDetailDto: UpdateCheckProductDetailDto) {
    return this.checkProductDetailsService.update(+id, updateCheckProductDetailDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkProductDetailsService.remove(+id);
  }
}
