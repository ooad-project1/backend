import { Test, TestingModule } from '@nestjs/testing';
import { CheckProductDetailsService } from './check_product_details.service';

describe('CheckProductDetailsService', () => {
  let service: CheckProductDetailsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckProductDetailsService],
    }).compile();

    service = module.get<CheckProductDetailsService>(CheckProductDetailsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
