import { IsNotEmpty } from "class-validator";
import { Product } from "src/products/entities/product.entity";
import { ManyToOne } from "typeorm";

export class CreateCheckProductDetailDto {
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    QtyLast: number;

    @IsNotEmpty()
    QtyRemain: number;

    @IsNotEmpty()
    QtyDamaged: string;

    @IsNotEmpty()
    productId: number;

    @IsNotEmpty()
    cpId: number;




}
