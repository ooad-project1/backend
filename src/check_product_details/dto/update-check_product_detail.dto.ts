import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckProductDetailDto } from './create-check_product_detail.dto';

export class UpdateCheckProductDetailDto extends PartialType(CreateCheckProductDetailDto) {}
