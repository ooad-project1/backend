import { Module } from '@nestjs/common';
import { ReceiptDetailService } from './receipt_detail.service';
import { ReceiptDetailController } from './receipt_detail.controller';
import { ReceiptDetail } from './entities/receipt_detail.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Product } from 'src/products/entities/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ReceiptDetail, Receipt, Product])],
  controllers: [ReceiptDetailController],
  providers: [ReceiptDetailService],
})
export class ReceiptDetailModule { }
