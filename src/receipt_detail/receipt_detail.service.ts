import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateReceiptDetailDto } from './dto/create-receipt_detail.dto';
import { UpdateReceiptDetailDto } from './dto/update-receipt_detail.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { ReceiptDetail } from './entities/receipt_detail.entity';
import { Repository } from 'typeorm';
import { Product } from 'src/products/entities/product.entity';

@Injectable()
export class ReceiptDetailService {
  constructor(
    @InjectRepository(ReceiptDetail)
    private receiptdetailRepository: Repository<ReceiptDetail>,
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
  ) { }

  async create(createReceiptDetailDto: CreateReceiptDetailDto) {
    return 'This action adds a new receiptDetail';
  }

  findAll() {
    return this.receiptdetailRepository.find({ relations: ['product'] });
  }

  async findOne(id: number) {
    const receiptDetail = await this.receiptdetailRepository.findOne({
      where: { id: id },
      relations: ['product'],
    });
    if (!receiptDetail) {
      throw new NotFoundException();
    }
    return receiptDetail;
  }

  async update(id: number, updateReceiptDetailDto: UpdateReceiptDetailDto) {
    return `This action updates a #${id} receiptDetail`;
  }

  async remove(id: number) {
    const receipt_detail = await this.receiptdetailRepository.findOneBy({
      id: id,
    });
    if (!receipt_detail) {
      throw new NotFoundException();
    }
    return this.receiptdetailRepository.remove(receipt_detail);
  }
}
