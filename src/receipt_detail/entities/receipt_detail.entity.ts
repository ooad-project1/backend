import { Product } from 'src/products/entities/product.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ReceiptDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ default: 0 })
  amount: number;

  @Column({ type: 'float', default: 0 })
  price: number;

  @Column({ type: 'float', default: 0 })
  total: number;

  @Column()
  productId: number;

  @ManyToOne(() => Receipt, (receipt) => receipt.receiptDetail)
  receipt: Receipt;

  @ManyToOne(() => Product, (product) => product.receiptDetail)
  product: Product;
}