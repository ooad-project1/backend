import { IsInt, IsNotEmpty, IsNumber, IsString, Min } from 'class-validator';

export class CreateReceiptDetailDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  amount: number;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  total: number;

  @IsNotEmpty()
  @IsInt()
  productId: number;

  @IsNotEmpty()
  @IsInt()
  receiptId: number;
}
