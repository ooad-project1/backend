import { IsNotEmpty } from "class-validator";
import { Double } from "typeorm";

export class CreateEmployeeDto {
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    lastName: string;

    @IsNotEmpty()
    workHourlyWage: number;

    @IsNotEmpty()
    userEmpId: number;

    @IsNotEmpty()
    role: string
}
