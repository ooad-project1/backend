import { Bill } from "src/bills/entities/bill.entity";
import { CheckProduct } from "src/check_products/entities/check_product.entity";
import { Receipt } from "src/receipts/entities/receipt.entity";
import { UserEmp } from "src/user_emp/entities/user_emp.entity";
import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Employee {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    lastName: string;

    @Column()
    workHourlyWage: number;

    @Column()
    userEmpId: number;

    @Column()
    role: string

    @OneToOne(() => UserEmp, (userEmp) => userEmp.employee)
    @JoinColumn()
    userEmp: UserEmp;

    @OneToMany(() => Receipt, (receipt) => receipt.employee)
    receipt: Receipt[];

    @OneToMany(() => Bill, (bill) => bill.employee)
    bill: Bill[];

    @OneToMany(() => CheckProduct, (checkProduct) => checkProduct.employee)
    checkProduct: CheckProduct[];
}
