import { Module } from '@nestjs/common';
import { EmployeeService } from './employee.service';
import { EmployeeController } from './employee.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEmp } from 'src/user_emp/entities/user_emp.entity';
import { Employee } from './entities/employee.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { Bill } from 'src/bills/entities/bill.entity';
import { CheckProduct } from 'src/check_products/entities/check_product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Employee, UserEmp,Receipt,Bill,CheckProduct])],
  controllers: [EmployeeController],
  providers: [EmployeeService]
})
export class EmployeeModule { }
