import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Employee } from './entities/employee.entity';
import { UserEmp } from 'src/user_emp/entities/user_emp.entity';

@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
    @InjectRepository(UserEmp)
    private userEmpRepository: Repository<UserEmp>,
  ) { }

  async create(createEmployeeDto: CreateEmployeeDto) {
    const newEmployee = new Employee();
    newEmployee.name = createEmployeeDto.name;
    newEmployee.lastName = createEmployeeDto.lastName;


    const userEmp = await this.userEmpRepository.findOne({
      where: { id: createEmployeeDto.userEmpId },
    });
    if (!userEmp) {
      throw new NotFoundException();
    }
    newEmployee.userEmp = userEmp;
    return this.employeesRepository.save(createEmployeeDto);
  }

  findAll() {
    return this.employeesRepository.find({
      select: {
        userEmp: { login: true, name: true, lastName: true, tel: true, email: true },
      },
      relations: ['userEmp']
    });
  }

  async findOne(id: number) {
    const employee = await this.employeesRepository.findOne({
      where: { id: id },
    });
    if (!employee) {
      throw new NotFoundException();
    }
    return employee;
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    const employee = await this.employeesRepository.findOneBy({ id: id });
    if (!employee) {
      throw new NotFoundException();
    }
    const updatedEmployee = { ...employee, ...updateEmployeeDto };
    return this.employeesRepository.save(updatedEmployee);
  }

  async remove(id: number) {
    const employee = await this.employeesRepository.findOneBy({ id: id });
    if (!employee) {
      throw new NotFoundException();
    }
    return this.employeesRepository.remove(employee);
  }
}
