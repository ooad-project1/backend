import { Customer } from 'src/customers/entities/customer.entity';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
    OneToOne,
} from 'typeorm';

@Entity()
export class UserCus {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    login: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    name: string;

    @Column()
    lastName: string;

    @Column({ length: '10' })
    tel: string;

    @OneToOne(() => Customer, (customer) => customer.userCus)
    customer: Customer;

    @Column()
    customerId: number;
}
