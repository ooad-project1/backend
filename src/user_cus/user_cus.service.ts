import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserCusDto } from './dto/create-user_cus.dto';
import { UpdateUserCusDto } from './dto/update-user_cus.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customer } from 'src/customers/entities/customer.entity';
import * as bcrypt from 'bcrypt';
import { UserCus } from './entities/user_cus.entity';

@Injectable()
export class UserCusService {
  constructor(
    @InjectRepository(UserCus)
    private userCusRepository: Repository<UserCus>,
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
  ) { }

  async create(createUserCusDto: CreateUserCusDto) {
    // const salt = await bcrypt.genSalt();
    // const hash = await bcrypt.hash(createUserCusDto.password, salt);

    const newUserCus = new UserCus();
    newUserCus.login = createUserCusDto.login;
    newUserCus.email = createUserCusDto.email;
    newUserCus.password = createUserCusDto.password
    newUserCus.name = createUserCusDto.name;
    newUserCus.lastName = createUserCusDto.lastName;
    newUserCus.tel = createUserCusDto.tel;

    const newCustomer = new Customer();
    newCustomer.name = createUserCusDto.name;
    newCustomer.lastName = createUserCusDto.lastName;
    newCustomer.tel = createUserCusDto.tel;
    newCustomer.startDate = new Date().toLocaleDateString();
    newCustomer.point = 0;
    const savedCustomer = await this.customerRepository.save(newCustomer);
    newUserCus.customerId = savedCustomer.id;
    newUserCus.customer = savedCustomer;
    return this.userCusRepository.save(newUserCus);
  }

  findAll() {
    return this.userCusRepository.find({
      relations: ['customer'],
    });
  }

  findOne(id: number) {
    return this.userCusRepository.findOne({ where: { id: id } });
  }

  findOneByEmail(email: string) {
    return this.userCusRepository.findOne({ where: { email: email } });
  }

  async update(id: number, updateUserCusDto: UpdateUserCusDto) {
    try {
      const updatedUserCus = await this.userCusRepository.save({
        id,
        ...updateUserCusDto,
      });
      return updatedUserCus;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {//ยังลบไม่ได้
    const userCus = await this.userCusRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedUserCus = await this.userCusRepository.remove(userCus);
      return deletedUserCus;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
