import { Module } from '@nestjs/common';
import { UserCusService } from './user_cus.service';
import { UserCusController } from './user_cus.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserCus } from './entities/user_cus.entity';
import { Customer } from 'src/customers/entities/customer.entity';

@Module({
  imports: [TypeOrmModule.forFeature([UserCus, Customer])],
  controllers: [UserCusController],
  providers: [UserCusService],
  exports: [UserCusService]
})
export class UserCusModule { }
