import { Test, TestingModule } from '@nestjs/testing';
import { UserCusController } from './user_cus.controller';
import { UserCusService } from './user_cus.service';

describe('UserCusController', () => {
  let controller: UserCusController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserCusController],
      providers: [UserCusService],
    }).compile();

    controller = module.get<UserCusController>(UserCusController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
