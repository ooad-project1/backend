import { Test, TestingModule } from '@nestjs/testing';
import { UserCusService } from './user_cus.service';

describe('UserCusService', () => {
  let service: UserCusService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserCusService],
    }).compile();

    service = module.get<UserCusService>(UserCusService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
