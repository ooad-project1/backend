import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { UserCusService } from './user_cus.service';
import { CreateUserCusDto } from './dto/create-user_cus.dto';
import { UpdateUserCusDto } from './dto/update-user_cus.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('user-cus')
export class UserCusController {
  constructor(private readonly userCusService: UserCusService) { }

  @Post()
  create(@Body() createUserCusDto: CreateUserCusDto) {
    return this.userCusService.create(createUserCusDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll() {
    return this.userCusService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userCusService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserCusDto: UpdateUserCusDto) {
    return this.userCusService.update(+id, updateUserCusDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userCusService.remove(+id);
  }
}
