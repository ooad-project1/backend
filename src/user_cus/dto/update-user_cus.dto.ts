import { PartialType } from '@nestjs/mapped-types';
import { CreateUserCusDto } from './create-user_cus.dto';

export class UpdateUserCusDto extends PartialType(CreateUserCusDto) {}
