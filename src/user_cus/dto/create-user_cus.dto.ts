import { IsNotEmpty, MinLength, Matches, IsEmail } from 'class-validator';

export class CreateUserCusDto {
  @IsNotEmpty()
  login: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  // @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  @MinLength(6)
  @IsNotEmpty()
  password: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  lastName: string;

  @IsNotEmpty()
  tel: string;

  // @IsNotEmpty()
  customerId: number;
}
