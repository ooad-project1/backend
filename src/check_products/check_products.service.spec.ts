import { Test, TestingModule } from '@nestjs/testing';
import { CheckProductsService } from './check_products.service';

describe('CheckProductsService', () => {
  let service: CheckProductsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckProductsService],
    }).compile();

    service = module.get<CheckProductsService>(CheckProductsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
