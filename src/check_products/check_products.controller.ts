import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CheckProductsService } from './check_products.service';
import { CreateCheckProductDto } from './dto/create-check_product.dto';
import { UpdateCheckProductDto } from './dto/update-check_product.dto';

@Controller('check-products')
export class CheckProductsController {
  constructor(private readonly checkProductsService: CheckProductsService) {}

  @Post()
  create(@Body() createCheckProductDto: CreateCheckProductDto) {
    return this.checkProductsService.create(createCheckProductDto);
  }

  @Get()
  findAll() {
    return this.checkProductsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkProductsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCheckProductDto: UpdateCheckProductDto) {
    return this.checkProductsService.update(+id, updateCheckProductDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkProductsService.remove(+id);
  }
}
