import { Module } from '@nestjs/common';
import { CheckProductsService } from './check_products.service';
import { CheckProductsController } from './check_products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckProduct } from './entities/check_product.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { CheckProductDetail } from 'src/check_product_details/entities/check_product_detail.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CheckProduct, Employee, CheckProductDetail])],
  controllers: [CheckProductsController],
  providers: [CheckProductsService]
})
export class CheckProductsModule { }
