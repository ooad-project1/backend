import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCheckProductDto } from './dto/create-check_product.dto';
import { UpdateCheckProductDto } from './dto/update-check_product.dto';
import { CheckProduct } from './entities/check_product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Employee } from 'src/employee/entities/employee.entity';

@Injectable()
export class CheckProductsService {
  constructor(
    @InjectRepository(CheckProduct)
    private checkProductsRepository: Repository<CheckProduct>,
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
    
  ) {}

  async create(createCheckProductDto: CreateCheckProductDto) {
    const employee = await this.employeesRepository.findOne({
      where: { 
        id: createCheckProductDto.employeeId,
      },
    });
    if (!employee) {
      throw new NotFoundException('Employee not found');
    }
    const newCheckProduct = new CheckProduct();
    newCheckProduct.dateTime = new Date().toLocaleDateString();
    newCheckProduct.employee = employee;
  }

  findAll() {
    return `This action returns all checkProducts`;
  }

  findOne(id: number) {
    return `This action returns a #${id} checkProduct`;
  }

  update(id: number, updateCheckProductDto: UpdateCheckProductDto) {
    return `This action updates a #${id} checkProduct`;
  }

  remove(id: number) {
    return `This action removes a #${id} checkProduct`;
  }
}
