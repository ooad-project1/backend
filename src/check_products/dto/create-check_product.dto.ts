import { IsNotEmpty } from "class-validator";

export class CreateCheckProductDto {
    @IsNotEmpty()
    dateTime: string;

    @IsNotEmpty()
    employeeId : number;
}
