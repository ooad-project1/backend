import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckProductDto } from './create-check_product.dto';

export class UpdateCheckProductDto extends PartialType(CreateCheckProductDto) {}
