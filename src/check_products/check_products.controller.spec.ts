import { Test, TestingModule } from '@nestjs/testing';
import { CheckProductsController } from './check_products.controller';
import { CheckProductsService } from './check_products.service';

describe('CheckProductsController', () => {
  let controller: CheckProductsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckProductsController],
      providers: [CheckProductsService],
    }).compile();

    controller = module.get<CheckProductsController>(CheckProductsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
