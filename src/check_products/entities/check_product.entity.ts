import { CheckProductDetail } from "src/check_product_details/entities/check_product_detail.entity";
import { Employee } from "src/employee/entities/employee.entity";
import { Column, CreateDateColumn, Entity, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class CheckProduct {
    @PrimaryGeneratedColumn()
    id: number;

    @CreateDateColumn()
    dateTime: string;

    @Column()
    employeeId: number;

    @ManyToOne(() => Employee, (employee) => employee.checkProduct)
    employee: Employee;

    @OneToMany(() => CheckProductDetail, (checkProductDetail) => checkProductDetail.checkProduct)
    checkProductDetail: CheckProductDetail[];
}
