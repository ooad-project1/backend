import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { Category } from 'src/categorys/entities/category.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Category)
    private categoriesRepository: Repository<Category>,
  ) { }

  async create(createProductDto: CreateProductDto) {
    const category = await this.categoriesRepository.findOne({
      where: {
        id: createProductDto.categoryId,
      },
    });
    const newProduct = new Product();
    newProduct.name = createProductDto.name;
    newProduct.image = createProductDto.image;
    newProduct.price = createProductDto.price;
    newProduct.author = createProductDto.author;
    newProduct.details = createProductDto.details;
    newProduct.status = createProductDto.status;
    newProduct.qtyStock = createProductDto.qtyStock;
    newProduct.minQty = createProductDto.minQty;
    newProduct.category = category;
    return this.productsRepository.save(newProduct);
  }

  findByCategory(id: number) {
    return this.productsRepository.find({ where: { categoryId: id } });
  }

  findAll() {
    return this.productsRepository.find({
      select: {
        category: { name: true },
      },
      relations: ['category']
    });
  }

  findOne(id: number) {
    return this.productsRepository.findOne({
      where: { id: id },
      select: {
        category: { name: true },
      },
      relations: ['category']
    });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productsRepository.findOne({
      where: { id: id },
    });
    if (!product) {
      throw new NotFoundException();
    }
    Object.assign(product, updateProductDto);
    return this.productsRepository.save(product);
  }

  async remove(id: number) {
    const product = await this.productsRepository.findOne({
      where: { id: id },
    });
    if (!product) {
      throw new NotFoundException();
    }
    return this.productsRepository.remove(product);
  }

  findByCategoryName(categoryId: number, name: string) {
    return this.productsRepository.find({
      where: { name: Like(`%${name}%`), categoryId: categoryId },
    });
  }

}
