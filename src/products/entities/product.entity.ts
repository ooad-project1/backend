import { BillDetail } from 'src/bill_details/entities/bill_detail.entity';
import { Bill } from 'src/bills/entities/bill.entity';
import { CartItem } from 'src/cart_items/entities/cart_item.entity';
import { Category } from 'src/categorys/entities/category.entity';
import { CheckProductDetail } from 'src/check_product_details/entities/check_product_detail.entity';
import { ReceiptDetail } from 'src/receipt_detail/entities/receipt_detail.entity';
import { Column, DeleteDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 64 })
  name: string;

  @Column()
  author: string;

  @Column({ length: '128', default: 'no_image_available.jpg' })
  image: string;

  @Column({ type: 'float' })
  price: number;

  @Column({ length: 1000 })
  details: string; // เพิ่ม '?' เพื่อทำให้ 'detail' เป็นตัวเลือก

  @Column({ length: 64 })
  status: string;

  @Column()
  qtyStock: number;

  @Column()
  minQty: number;

  @Column()
  categoryId: number;

  // @DeleteDateColumn()
  // delDate: Date;

  @ManyToOne(() => Category, (category) => category.product)
  category: Category;

  @OneToMany(() => BillDetail, (billDetail) => billDetail.product)
  billDetails: BillDetail[];

  @OneToMany(() => CheckProductDetail, (checkProductDetail) => checkProductDetail.product)
  checkProductDetail: CheckProductDetail[];

  @OneToMany(() => CartItem, (cartItem) => cartItem.product)
  cartItem: CartItem[];

  @OneToMany(() => ReceiptDetail, (receiptDetail) => receiptDetail.product)
  receiptDetail: ReceiptDetail[];
}
