import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Category } from 'src/categorys/entities/category.entity';
import { Bill } from 'src/bills/entities/bill.entity';
import { BillDetail } from 'src/bill_details/entities/bill_detail.entity';
import { CheckProductDetail } from 'src/check_product_details/entities/check_product_detail.entity';
import { ReceiptDetail } from 'src/receipt_detail/entities/receipt_detail.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Product, Category, Bill, BillDetail, CheckProductDetail])],
  controllers: [ProductsController],
  providers: [ProductsService],
})
export class ProductsModule { }
