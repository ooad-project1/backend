import {
  IsNotEmpty,
  Length,
  IsPositive,
  IsString,
  MinLength,
  MaxLength,
  IsInt,
  Min,
} from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 64)
  @IsString()
  name: string;

  @IsNotEmpty()

  price: number;

  @IsNotEmpty()
  author: string;

  image = 'no_image_available.jpg';

  @IsNotEmpty()
  details: string;

  @IsNotEmpty()
  @IsString()
  status: string;

  @IsNotEmpty()
  qtyStock: number;

  @IsNotEmpty()
  minQty: number;

  @IsNotEmpty()
  categoryId: number;
}
