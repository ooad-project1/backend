import { Module } from '@nestjs/common';
import { PromotionsService } from './promotions.service';
import { PromotionsController } from './promotions.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Promotion } from './entities/promotion.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Promotion, Receipt])],
  controllers: [PromotionsController],
  providers: [PromotionsService]
})
export class PromotionsModule { }
