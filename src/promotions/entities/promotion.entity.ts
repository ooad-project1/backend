import { Receipt } from "src/receipts/entities/receipt.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Promotion {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    code: string;

    @Column()
    discount: number;

    @OneToMany(() => Receipt, (receipt) => receipt.promotion)
    receipt: Receipt[];



}
