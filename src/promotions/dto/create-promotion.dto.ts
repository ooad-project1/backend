import { IsInt, IsNotEmpty, IsPositive, IsString, Length } from "class-validator";

export class CreatePromotionDto {
    @IsNotEmpty()
    @Length(3, 64)
    @IsString()
    name: string;

    @IsNotEmpty()
    code: string

    @IsNotEmpty()
    @IsPositive()
    @IsInt()
    discount: number;

}
