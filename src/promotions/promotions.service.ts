import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Promotion } from './entities/promotion.entity';

@Injectable()
export class PromotionsService {
  constructor(
    @InjectRepository(Promotion)
    private promotionRepository: Repository<Promotion>,
  ) { }

  async create(createPromotionDto: CreatePromotionDto): Promise<Promotion> {
    const newPromotion = new Promotion();
    newPromotion.code = createPromotionDto.code;
    newPromotion.discount = createPromotionDto.discount;
    newPromotion.name = createPromotionDto.name;
    return await this.promotionRepository.save(newPromotion);
  }

  findAll() {
    return this.promotionRepository.find({});
  }

  async findOne(id: number) {
    const promotion = await this.promotionRepository.findOne({
      where: { id: id },
    });
    if (!promotion) {
      throw new NotFoundException();
    }
    return promotion;
  }

  async findOneByCode(code: string) {
    const promotion = await this.promotionRepository.findOne({
      where: { code: code },
    });
    if (!promotion) {
      throw new NotFoundException();
    }
    return promotion;
  }

  update(id: number, updatePromotionDto: UpdatePromotionDto) {
    return `This action updates a #${id} promotion`;
  }

  remove(id: number) {
    return `This action removes a #${id} promotion`;
  }
}
