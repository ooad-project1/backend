import { PartialType } from '@nestjs/mapped-types';
import { CreateUserEmpDto } from './create-user_emp.dto';

export class UpdateUserEmpDto extends PartialType(CreateUserEmpDto) {}
