import { Test, TestingModule } from '@nestjs/testing';
import { UserEmpController } from './user_emp.controller';
import { UserEmpService } from './user_emp.service';

describe('UserEmpController', () => {
  let controller: UserEmpController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserEmpController],
      providers: [UserEmpService],
    }).compile();

    controller = module.get<UserEmpController>(UserEmpController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
