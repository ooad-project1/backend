import { Employee } from 'src/employee/entities/employee.entity';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    OneToOne,
} from 'typeorm';

@Entity()
export class UserEmp {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    login: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    name: string;

    @Column()
    lastName: string;

    @Column({ length: '10' })
    tel: string;

    @Column()
    role: string

    @Column()
    employeeId: number;

    @OneToOne(() => Employee, (employee) => employee.userEmp)
    employee: Employee;
}

