import { Module } from '@nestjs/common';
import { UserEmpService } from './user_emp.service';
import { UserEmpController } from './user_emp.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEmp } from './entities/user_emp.entity';
import { Employee } from 'src/employee/entities/employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([UserEmp, Employee])],
  controllers: [UserEmpController],
  providers: [UserEmpService],
  exports: [UserEmpService]
})
export class UserEmpModule { }
