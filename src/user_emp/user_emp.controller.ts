import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { UserEmpService } from './user_emp.service';
import { CreateUserEmpDto } from './dto/create-user_emp.dto';
import { UpdateUserEmpDto } from './dto/update-user_emp.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('user-emp')
export class UserEmpController {
  constructor(private readonly userEmpService: UserEmpService) { }

  @Post()
  create(@Body() createUserEmpDto: CreateUserEmpDto) {
    return this.userEmpService.create(createUserEmpDto);
  }

  @UseGuards(JwtAuthGuard) //ปิดเป็นคอมเม้นถ้าจะดู
  @Get()
  findAll() {
    return this.userEmpService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userEmpService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserEmpDto: UpdateUserEmpDto) {
    return this.userEmpService.update(+id, updateUserEmpDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userEmpService.remove(+id);
  }
}
