import { Injectable, NotFoundException } from '@nestjs/common';
import { UpdateUserEmpDto } from './dto/update-user_emp.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { Double, Repository } from 'typeorm';
import { UserEmp } from './entities/user_emp.entity';
import { CreateUserEmpDto } from './dto/create-user_emp.dto';

@Injectable()
export class UserEmpService {
  constructor(
    @InjectRepository(UserEmp)
    private userEmpRepository: Repository<UserEmp>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) { }

  async create(createUserEmpDto: CreateUserEmpDto) {
    // const salt = await bcrypt.genSalt();
    // const hash = await bcrypt.hash(createUserEmpDto.password, salt);

    const newUserEmp = new UserEmp();
    newUserEmp.login = createUserEmpDto.login;
    newUserEmp.email = createUserEmpDto.email;
    newUserEmp.password = createUserEmpDto.password
    newUserEmp.name = createUserEmpDto.name;
    newUserEmp.lastName = createUserEmpDto.lastName;
    newUserEmp.tel = createUserEmpDto.tel;
    newUserEmp.role = createUserEmpDto.role;

    const newEmployee = new Employee();
    newEmployee.name = createUserEmpDto.name;
    newEmployee.lastName = createUserEmpDto.lastName;
    newEmployee.workHourlyWage = 0.0;
    newEmployee.role = createUserEmpDto.role;
    const savedEmployee = await this.employeeRepository.save(newEmployee);
    newUserEmp.employeeId = savedEmployee.id;
    newUserEmp.employee = savedEmployee;
    return this.userEmpRepository.save(newUserEmp);
  }

  findAll() {
    return this.userEmpRepository.find({
      relations: ['employee'],
    });
  }

  findOne(id: number) {
    return this.userEmpRepository.findOne({ where: { id: id } });
  }

  findOneByEmail(email: string) {
    return this.userEmpRepository.findOne({ where: { email: email } });
  }

  async update(id: number, updateUserEmpDto: UpdateUserEmpDto) {
    try {
      const updatedUserEmp = await this.userEmpRepository.save({
        id,
        ...updateUserEmpDto,
      });
      return updatedUserEmp;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const userEmp = await this.userEmpRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedUserEmp = await this.userEmpRepository.remove(userEmp);
      return deletedUserEmp;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}