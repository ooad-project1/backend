import { Test, TestingModule } from '@nestjs/testing';
import { UserEmpService } from './user_emp.service';

describe('UserEmpService', () => {
  let service: UserEmpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserEmpService],
    }).compile();

    service = module.get<UserEmpService>(UserEmpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
