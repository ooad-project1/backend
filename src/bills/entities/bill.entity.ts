import { BillDetail } from "src/bill_details/entities/bill_detail.entity";
import { Employee } from "src/employee/entities/employee.entity";
import { Product } from "src/products/entities/product.entity";
import { Vendor } from "src/vendors/entities/vendor.entity";
import { VendorsController } from "src/vendors/vendors.controller";
import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Bill {
    @PrimaryGeneratedColumn()
    id: number;

    @CreateDateColumn()
    dateTime: string;

    @Column({ type: 'float' })
    total: number;

    @Column()
    buy: number;

    @Column()
    change: number;

    @Column()
    vendorId: number;

    @Column()
    employeeId: number;

    @ManyToOne(() => Vendor, (vendor) => vendor.bill)
    vendor: Vendor;

    @OneToMany(() => BillDetail, (billDetail) => billDetail.bill)
    billDetail: BillDetail[];

    @ManyToOne(() => Employee, (employee) =>  employee.bill)
    employee: Employee;
}
