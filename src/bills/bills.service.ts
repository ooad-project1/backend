import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { Bill } from './entities/bill.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BillDetail } from 'src/bill_details/entities/bill_detail.entity';
import { Vendor } from 'src/vendors/entities/vendor.entity';
import { Like } from 'typeorm';
import { Employee } from 'src/employee/entities/employee.entity';

@Injectable()
export class BillsService {
  constructor(
    @InjectRepository(Bill)
    private billsRepository: Repository<Bill>,
    @InjectRepository(Vendor)
    private vendorsRepository: Repository<Vendor>,
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>
  ) { }



  async create(createBillDto: CreateBillDto) {
    const vendor = await this.vendorsRepository.findOneBy({
      id: createBillDto.vendorId,
    });
    const employee = await this.employeesRepository.findOneBy({
      id: createBillDto.employeeId,
    });

    const newBill = new Bill();
    newBill.dateTime = new Date().toLocaleDateString();;
    newBill.total = createBillDto.total;
    newBill.buy = createBillDto.buy;
    newBill.vendor = vendor;
    newBill.employee = employee;
    return this.billsRepository.save(newBill);
  }

  findAll() {
    return this.billsRepository.find({
      select: {
        vendor: { name: true },
        employee: { name: true },
      },
      relations: ['vendor', 'employee'],

    });
  }

  async findOne(id: number) {
    const bill = await this.billsRepository.findOne({
      where: { id: id },

    });
    if (!bill) {
      throw new NotFoundException();
    }
    return bill;
  }




  async update(id: number, updateBillDto: UpdateBillDto) {
    try {
      const updatedBill = await this.billsRepository.save({
        id,
        ...updateBillDto,
      });
      return updatedBill;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const bill = await this.billsRepository.findOneBy({ id: id });
    if (!bill) {
      throw new NotFoundException();
    }
    return this.billsRepository.softRemove(bill);
  }
  // findByVendorId(vendorId: number {
  //   return this.billsRepository.find({
  //     where: {
  //       vendor: { id: vendorId },
  //       // สมมติว่าชื่อของ Vendor อยู่ใน attribute name ของ Entity Vendor
  //       // และเราต้องการค้นหาชื่อ Vendor ที่มีส่วนของชื่อตรงกับ name ที่ระบุ
  //       // ดังนั้นเราใช้ Like ในการค้นหาชื่อของ Vendor
  //       // หากชื่อ Vendor อยู่ใน attribute อื่น ๆ ให้แก้ไขตามลำดับ
  //       name: Like(`%${name}%`)
  //     },
  //     relations: ['vendor'], // เพิ่ม relations เพื่อให้สามารถเข้าถึงข้อมูลของ Vendor ได้
  //   });
  // }


  async findByVendorId(vendorId: number) {
    console.log(vendorId);
    const receipt = await this.billsRepository.findOne({
      where: { vendorId: vendorId },
    });
    if (!vendorId) {
      throw new NotFoundException();
    }
    return receipt;
  }

}
