import { Module } from '@nestjs/common';
import { BillsService } from './bills.service';
import { BillsController } from './bills.controller';
import { Bill } from './entities/bill.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from 'src/products/entities/product.entity';
import { Vendor } from 'src/vendors/entities/vendor.entity';
import { BillDetail } from 'src/bill_details/entities/bill_detail.entity';
import { Employee } from 'src/employee/entities/employee.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Bill, Product, Vendor, BillDetail,Employee])],
  controllers: [BillsController],
  providers: [BillsService],
})
export class BillsModule { }
