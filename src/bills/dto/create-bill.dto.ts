import { IsNotEmpty } from "class-validator";
import { Product } from "src/products/entities/product.entity";
import { ManyToOne, OneToMany } from "typeorm";

export class CreateBillDto {

    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    dateTime: string;

    @IsNotEmpty()
    total: number;

    @IsNotEmpty()
    buy: number;

    @IsNotEmpty()
    change: number;

    @IsNotEmpty()
    vendorId: number;

    @IsNotEmpty()
    employeeId: number;




}
