import { Module } from '@nestjs/common';
import { VendorsService } from './vendors.service';
import { VendorsController } from './vendors.controller';
import { Vendor } from './entities/vendor.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Bill } from 'src/bills/entities/bill.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Vendor,Bill])],
  controllers: [VendorsController],
  providers: [VendorsService],
})
export class VendorsModule { }
