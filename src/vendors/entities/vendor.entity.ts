import { Bill } from 'src/bills/entities/bill.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Vendor {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @OneToMany(() => Bill, (bill) => bill.vendor)
    bill: Bill[];
}
