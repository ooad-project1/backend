import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateVendorDto } from './dto/create-vendor.dto';
import { UpdateVendorDto } from './dto/update-vendor.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Vendor } from './entities/vendor.entity';
import { Repository } from 'typeorm';

@Injectable()
export class VendorsService {
  constructor(
    @InjectRepository(Vendor)
    private vendorsRepository: Repository<Vendor>,
  ) { }
  create(createVendorDto: CreateVendorDto) {
    return this.vendorsRepository.save(createVendorDto);
  }

  findAll() {
    return this.vendorsRepository.find();
  }

  async findOne(id: number) {
    const vendor = await this.vendorsRepository.findOne({
      where: { id: id },
    });
    if (!vendor) {
      throw new NotFoundException();
    }
    return vendor;
  }

  async update(id: number, updateVendorDto: UpdateVendorDto) {
    const vendor = await this.vendorsRepository.findOneBy({ id: id });
    if (!vendor) {
      throw new NotFoundException();
    }
    const updatedVendor = { ...vendor, ...updateVendorDto };
    return this.vendorsRepository.save(updatedVendor);
  }

  async remove(id: number) {
    const vendor = await this.vendorsRepository.findOneBy({ id: id });
    if (!vendor) {
      throw new NotFoundException();
    }
    return this.vendorsRepository.softRemove(vendor);
  }
}
