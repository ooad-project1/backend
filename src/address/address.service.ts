import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAddressDto } from './dto/create-address.dto';
import { UpdateAddressDto } from './dto/update-address.dto';
import { Address } from './entities/address.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Like } from "typeorm";

@Injectable()
export class AddressService {
  constructor(
    @InjectRepository(Address)
    private addressRepository: Repository<Address>,
  ) { }
  async create(createAddressDto: CreateAddressDto): Promise<Address> {
    const newAddress  =  new Address();
    newAddress.name = createAddressDto.name;
    newAddress.lastName = createAddressDto.lastName;
    newAddress.receiverTel = createAddressDto.receiverTel;
    newAddress.detail = createAddressDto.detail;
    newAddress.province = createAddressDto.province;
    newAddress.district = createAddressDto.district;
    newAddress.subDistrict = createAddressDto.subDistrict;
    newAddress.postCode = createAddressDto.postCode;
    return this.addressRepository.save(newAddress);
  }

  findAll() {
    return this.addressRepository.find({});
  }

  findOne(id: number) {
    return this.addressRepository.findOne({
      where: { id: id },
    });
  }

  async update(id: number, updateAddressDto: UpdateAddressDto) {
    const address = await this.addressRepository.findOneBy({ id: id });
    if (!address) {
      throw new NotFoundException();
    }
    const updatedAddress = { ...address, ...updateAddressDto };
    return this.addressRepository.save(updatedAddress);
  }

  async remove(id: number) {
    const address = await this.addressRepository.findOneBy({ id: id });
    if (!address) {
      throw new NotFoundException();
    }
    return this.addressRepository.softRemove(address);
  }
  
  async findByReceiverTel(receiverTel: string) {
    return this.addressRepository.find({
      where: { receiverTel: Like(`%${receiverTel}%`) },
    });
  }


}

// findByReceiverTel(addressId: number, name: string) {
  //   return this.addressRepository.find({
  //     where: { name: Like(`%${name}%`), addressId: addressId },
  //   });
  // }
