import { Receipt } from "src/receipts/entities/receipt.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Address {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: String

    @Column()
    lastName: String

    @Column()
    receiverTel: String

    @Column()
    detail: String

    @Column()
    province: String

    @Column()
    district: String

    @Column()
    subDistrict: String

    @Column()
    postCode: String

    @OneToMany(() => Receipt, (receipt) => receipt.address)
    receipt: Receipt[];
}
