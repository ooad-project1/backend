import { IsNotEmpty } from "class-validator"

export class CreateAddressDto {


    @IsNotEmpty()
    name: String

    @IsNotEmpty()
    lastName: String

    @IsNotEmpty()
    receiverTel: String

    @IsNotEmpty()
    detail: String

    @IsNotEmpty()
    province: String

    @IsNotEmpty()
    district: String

    @IsNotEmpty()
    subDistrict: String

    @IsNotEmpty()
    postCode: String
}
