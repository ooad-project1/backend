import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Repository } from 'typeorm';
import { Product } from 'src/products/entities/product.entity';
import { ReceiptDetail } from 'src/receipt_detail/entities/receipt_detail.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { Address } from 'src/address/entities/address.entity';
import { Promotion } from 'src/promotions/entities/promotion.entity';
import { CreatePromotionDto } from 'src/promotions/dto/create-promotion.dto';

@Injectable()
export class ReceiptsService {
  constructor(
    @InjectRepository(Receipt)
    private receiptsRepository: Repository<Receipt>,
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
    @InjectRepository(Address)
    private addressRepository: Repository<Address>,
    @InjectRepository(Promotion)
    private promotionsRepository: Repository<Promotion>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(ReceiptDetail)
    private receiptDetailsRepository: Repository<ReceiptDetail>,
  ) {}

  async create(createReceiptDto: CreateReceiptDto) {
    const customer = await this.customersRepository.findOneBy({
      id: createReceiptDto.customerId,
    });
    const employee = await this.employeesRepository.findOneBy({
      id: createReceiptDto.employeeId,
    });
    const address = await this.addressRepository.findOneBy({
      id: createReceiptDto.addressId,
    });
    const promotion = await this.promotionsRepository.findOneBy({
      id: createReceiptDto.promotionId,
    });

    const newReceipt: Receipt = new Receipt();

    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, '0');
    const day = String(currentDate.getDate()).padStart(2, '0');
    const hours = String(currentDate.getHours()).padStart(2, '0');
    const minutes = String(currentDate.getMinutes()).padStart(2, '0');
    const seconds = String(currentDate.getSeconds()).padStart(2, '0');
    const formattedDateTime = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
    newReceipt.dateTime = formattedDateTime;
    // newReceipt.dateTime = new Date().toLocaleDateString();
    newReceipt.customer = customer;
    newReceipt.address = address;
    newReceipt.employee = employee;
    newReceipt.promotion = promotion;
    newReceipt.cash = createReceiptDto.cash;
    newReceipt.status = createReceiptDto.status;
    newReceipt.channel = createReceiptDto.channel;
    newReceipt.paymentMethod = createReceiptDto.paymentMethod;
    newReceipt.shippcost = createReceiptDto.shippcost;
    newReceipt.discount = createReceiptDto.discount;
    newReceipt.totalPrice = 0;
    newReceipt.slipImage = createReceiptDto.slipImage;

    const savedReceipt = await this.receiptsRepository.save(newReceipt);
    newReceipt.id = savedReceipt.id;
    for (const item of createReceiptDto.receiptDetails) {
      const receiptDetail = new ReceiptDetail();
      receiptDetail.amount = item.amount;
      receiptDetail.product = await this.productsRepository.findOneBy({
        id: item.productId,
      });
      receiptDetail.name = receiptDetail.product.name;
      receiptDetail.price = receiptDetail.product.price;
      receiptDetail.total = receiptDetail.price * receiptDetail.amount;
      receiptDetail.receipt = newReceipt;
      newReceipt.totalPrice = newReceipt.totalPrice + receiptDetail.total;
      await this.receiptDetailsRepository.save(receiptDetail);
    }

    savedReceipt.totalPrice =
      savedReceipt.totalPrice + savedReceipt.shippcost - newReceipt.discount;
    if (newReceipt.channel == 'online') {
      savedReceipt.change = 0;
    } else {
      savedReceipt.change = savedReceipt.cash - savedReceipt.totalPrice;
    }

    await this.receiptsRepository.save(savedReceipt);
    return await this.receiptsRepository.findOne({
      where: { id: newReceipt.id },
      relations: ['receiptDetail'],
    });
  }

  findAll() {
    return this.receiptsRepository.find({
      relations: ['customer', 'receiptDetail', 'address', 'employee'],
    });
  }

  findOne(id: number) {
    return this.receiptsRepository.findOne({
      where: { id: id },
      relations: ['customer', 'receiptDetail', 'address', 'employee'],
      select: { customer: { tel: true }, employee: { name: true } },
    });
  }

  // async updateStatus(id: number, updateReceiptDto: UpdateReceiptDto) {
  //   const existingReceipt = await this.receiptsRepository.findOne(id, {
  //     relations: ['customer', 'receiptDetail', 'address', 'employee'],
  //   });

  //   if (!existingReceipt) {
  //     throw new NotFoundException(`Receipt with ID ${id} not found`);
  //   }

  //   // Update only the status property
  //   if (updateReceiptDto.status !== undefined) {
  //     existingReceipt.status = updateReceiptDto.status;
  //   }

  //   // Save the updated receipt entity
  //   await this.receiptsRepository.save(existingReceipt);

  //   // Optionally, you can return the updated receipt
  //   return existingReceipt;
  // }

  async update(id: number, updateReceiptDto: UpdateReceiptDto) {
    const receipt = await this.receiptsRepository.findOneBy({ id: id });
    if (!receipt) {
      throw new NotFoundException();
    }
    const updatedreceipt = { ...receipt, ...updateReceiptDto };
    return this.receiptsRepository.save(updatedreceipt);
  }
  async remove(id: number) {
    const order = await this.receiptsRepository.findOneBy({ id: id });
    return this.receiptsRepository.softRemove(order);
  }

  async findByChannel(channel: string) {
    console.log(channel);
    const receipt = await this.receiptsRepository.findOne({
      where: { channel: channel },
    });
    if (!receipt) {
      throw new NotFoundException();
    }
    return receipt;
  }
}
