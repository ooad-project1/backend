
import { Address } from 'src/address/entities/address.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { Promotion } from 'src/promotions/entities/promotion.entity';
import { ReceiptDetail } from 'src/receipt_detail/entities/receipt_detail.entity';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Receipt {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dateTime: string;

  @Column()
  totalPrice: number;

  @Column({ type: 'float', default: 0 })
  cash: number;

  @Column({ type: 'float', default: 0 })
  change: number;

  @Column()
  status: string;

  @Column()
  channel: string;

  @Column()
  paymentMethod: string;

  @Column({ default: 0 })
  shippcost: number;

  @Column({ type: 'float', default: 0 })
  discount: number;

  @Column()
  customerId: number;

  @Column()
  employeeId: number;

  @Column()
  addressId: number;

  @Column()
  promotionId: number;

  @Column({ length: '128', default: 'no_image_available.jpg' })
  slipImage: string;

  @ManyToOne(() => Customer, (customer) => customer.receipts)
  customer: Customer;

  @ManyToOne(() => Address, (address) => address.receipt)
  address: Address;

  @OneToMany(() => ReceiptDetail, (receiptDetail) => receiptDetail.receipt)
  receiptDetail: ReceiptDetail[];

  @ManyToOne(() => Employee, (employee) => employee.receipt)
  employee: Employee;

  @ManyToOne(() => Promotion, (promotion) => promotion.receipt)
  promotion: Promotion;
}
