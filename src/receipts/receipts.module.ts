import { Module } from '@nestjs/common';
import { ReceiptsService } from './receipts.service';
import { ReceiptsController } from './receipts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { ReceiptDetail } from 'src/receipt_detail/entities/receipt_detail.entity';
import { Address } from 'src/address/entities/address.entity';
import { Promotion } from 'src/promotions/entities/promotion.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { Product } from 'src/products/entities/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Receipt, Customer, Employee, ReceiptDetail, Address, Promotion, Product])],
  controllers: [ReceiptsController],
  providers: [ReceiptsService],
})
export class ReceiptsModule { }
