import {
  IsNotEmpty,
  IsNumber,
  IsDateString,
  IsString,
  IsPositive,
  Min,
  IsInt,
  IsDate,
} from 'class-validator';
import { CreateReceiptDetailDto } from 'src/receipt_detail/dto/create-receipt_detail.dto';

export class CreateReceiptDto {

  // @IsNotEmpty()
  // @IsDate()
  dateTime: string;

  // @IsNotEmpty()
  // @IsPositive()
  totalPrice: number;

  // @IsNotEmpty()
  // @IsPositive()
  cash: number;

  // @IsNotEmpty()
  // @IsNumber()
  change: number;

  // @IsNotEmpty()
  // @IsString()
  status: string;

  // @IsNotEmpty()
  // @IsString()
  channel: string;

  // @IsNotEmpty()
  // @IsString()
  paymentMethod: string;

  // @IsNotEmpty()
  // @IsPositive()
  shippcost: number;

  // @IsNotEmpty()
  // @IsNumber()
  discount: number;

  @IsNotEmpty()
  @IsInt()
  customerId: number;

  @IsNotEmpty()
  @IsInt()
  employeeId: number;

  @IsNotEmpty()
  @IsInt()
  addressId: number;

  // @IsNotEmpty()
  @IsInt()
  promotionId: number;

  @IsNotEmpty()
  receiptDetails: CreateReceiptDetailDto[];

  slipImage: string;

}
