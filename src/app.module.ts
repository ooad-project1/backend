import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductsModule } from './products/products.module';
import { ReceiptsModule } from './receipts/receipts.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesModule } from './roles/roles.module';
import { Product } from './products/entities/product.entity';
import { Receipt } from './receipts/entities/receipt.entity';
import { Role } from './roles/entities/role.entity';
import { CustomersModule } from './customers/customers.module';
import { Customer } from './customers/entities/customer.entity';
import { CategorysModule } from './categorys/categorys.module';
import { Category } from './categorys/entities/category.entity';
import { DataSource } from 'typeorm';
import { UserCusModule } from './user_cus/user_cus.module';
import { UserCus } from './user_cus/entities/user_cus.entity';
import { BillsModule } from './bills/bills.module';
import { VendorsModule } from './vendors/vendors.module';
import { Vendor } from './vendors/entities/vendor.entity';
import { ReceiptDetail } from './receipt_detail/entities/receipt_detail.entity';
import { ReceiptDetailModule } from './receipt_detail/receipt_detail.module';
import { Bill } from './bills/entities/bill.entity';
import { EmployeeModule } from './employee/employee.module';
import { AddressModule } from './address/address.module';
import { Address } from './address/entities/address.entity';
import { UserEmp } from './user_emp/entities/user_emp.entity';
import { UserEmpModule } from './user_emp/user_emp.module';
import { BillDetailsModule } from './bill_details/bill_details.module';
import { Employee } from './employee/entities/employee.entity';
import { BillDetail } from './bill_details/entities/bill_detail.entity';
import { PromotionsModule } from './promotions/promotions.module';
import { Promotion } from './promotions/entities/promotion.entity';
import { AuthModule } from './auth/auth.module';
import { CheckProductsModule } from './check_products/check_products.module';
import { CheckProduct } from './check_products/entities/check_product.entity';
import { CheckProductDetailsModule } from './check_product_details/check_product_details.module';
import { CheckProductDetail } from './check_product_details/entities/check_product_detail.entity';
import { CartItemsModule } from './cart_items/cart_items.module';
import { CartItem } from './cart_items/entities/cart_item.entity';

require('dotenv').config();

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: `${process.env.DB_IP}`,
      port: 3306,
      username: `${process.env.DB_USERNAME}`,
      password: `${process.env.DB_PASSWORD}`,
      database: `${process.env.DB_DB}`,
      entities: [
        Product,
        Receipt,
        UserCus,
        Role,
        Customer,
        Category,
        ReceiptDetail,
        Vendor,
        Bill,
        Address,
        UserEmp,
        Employee,
        BillDetail,
        Promotion,
        CheckProduct,
        CheckProductDetail,
        CartItem
      ],
      synchronize: true,
    }),
    ProductsModule,
    ReceiptsModule,
    RolesModule,
    CustomersModule,
    CategorysModule,
    UserCusModule,
    VendorsModule,
    ReceiptDetailModule,
    EmployeeModule,
    AddressModule,
    UserEmpModule,
    BillDetailsModule,
    PromotionsModule,
    AuthModule,
    CheckProductsModule,
    BillsModule,
    CheckProductDetailsModule,
    CartItemsModule,
    CartItemsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) { }
}
